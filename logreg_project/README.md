# Federated GWAS App for the FeatureCloud Platform

This FeatureCloud app is based on the [FeatureCloud Flask Template](https://github.com/FeatureCloud/flask_template). 

### Usage

### Technical Details

 - This app overwrites the api.py and web.py of the [FeatureCloud Flask Template](https://github.com/FeatureCloud/flask_template).
 - In the templates directory are the html templates for this app
 - In the requirements.txt are the project specific python requirements which will be installed in the docker image via Dockerfile
 - The build.sh automatically builds the Federated Mean App with the image name fc_mean
 