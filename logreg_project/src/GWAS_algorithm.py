##!/usr/bin/python3

# This code implements the methods of both server and client in federated GWAS as to be used by FeatureCloud.
# This is still only the algorithm. The communication with the server is already implemented in the flask template.
# Parts of this code are adapted from the official sPLINK source code.

#The Main method runs a single beta iteration and aggregation

# imports needed by client
import numpy as np
import math
import pathos.multiprocessing as mp  # absolutely use pathos. Otherwise the multiprocessing will not work on Windows.

# import needed by server
from operator import add
from scipy.stats import chi2


class Client:

    def __init__(self, server, bed_path, fam_path, cov_path, bim_path, confounding_features, number_of_chunks,
                 algorithm):

        self.bed_path = bed_path
        self.fam_path = fam_path
        self.cov_path = cov_path
        self.bim_path = bim_path

        self.confounding_features = confounding_features.split(",")
        self.number_of_chunks = number_of_chunks
        self.algorithm = algorithm

        # initialize attributes that will be used later on
        self.bed_file = None
        self.fam_file = None
        self.cov_file = None
        self.bim_file = None

        self.phenotype_vector = []
        self.sample_IIDs = []
        self.SNP_values = []
        self.feature_values = [[]]
        self.beta = {}
        self.SNP_names = []
        self.first_alleles = []
        self.second_alleles = []
        self.global_minor_allele_names = {}

        # here, each number in the list refers to the starting point of a new chunk.
        # the list is created in split_SNPs_into_chunks()
        self.chunk_starting_points = [0]

        # attributes needed for reading the .bed file
        self.byte_list = []
        self.per_SNP_byte_count = 0

        self.bin_genotype_to_dec = {"00": 0,  # homozygous, 1. allele
                                    "01": 1,  # missing value
                                    "10": 2,  # heterozygous
                                    "11": 3  # homozygous, 2. allele
                                    }

        # 1. Initialize
        # TODO: change this later to match the flask template
        self.server = server
        print("initialize")
        self.initialize(self.bed_path, self.fam_path, self.cov_path, self.bim_path, self.confounding_features)

        # 2. extract data from .bim file
        print("processing bim")
        self.process_bim(self.bim_file)

        # close all files
        self.bed_file.close()
        self.fam_file.close()
        self.cov_file.close()
        self.bim_file.close()

        # 3. calculate local sample count (n)
        print("local_sample_count")
        self.local_sample_count = len(self.phenotype_vector)

        # calculate chunks
        self.split_SNPs_into_chunks(self.SNP_names, self.number_of_chunks)

        # 4.1 calculate non-missing sample count
        # a '-9' in the phenotype vector encodes a missing sample
        print("missing samples")
        self.non_missing_sample_count = self.get_sample_count() - self.phenotype_vector.count('-9')

        # 4.2 start parallelization & allele count
        # will be most effective if number_of_chunks == number of processors in your pc
        # to check this, run: print(mp.cpu_count()) (after importing multiprocessing as mp)
        #
        # simple test to check if parallelization works in general:
        #
        # from math import cos
        # p = mp.Pool(2)
        # results = p.map(cos, range(10))
        # print(results)
        print("parallel allele count")
        pool = mp.Pool(self.number_of_chunks)

        # parallel step 1: allele count
        parallel_allele_counts = pool.map(self.calc_local_allele_count, range(self.number_of_chunks))
        # parallel allele counts has the values (first_allele_vector, second_allele_vector, chunk_no) for each chunk

        # 5. Minor alleles
        # reformat allele counts and add their names to send to the server
        server_input_allele_counts = self.get_allele_count_format_for_server(parallel_allele_counts)
        # server_input_allele_counts lists SNP_names, first_allele_names, second_allele_names, first_allele_counts and
        # second allele counts as separate fields one after another.

        # send allele counts to server
        # TODO: enable communication with server according to flask template
        # temporary solution:
        self.global_minor_allele_names = self.server.get_global_minor_alleles(server_input_allele_counts)

        # 5. - 6. process global minor alleles and create contingency tables, parallel step 2
        print("algorithm running")
        print(self.SNP_values)
        for SNP_name in self.SNP_names:
            self.beta[SNP_name] = np.zeros((2 + len(self.feature_values), 1))
        chunk_results = pool.map(self.swap_values_and_start_algorithm, range(self.number_of_chunks))
        pool.close()
        # send results to server
        # TODO: enable communication with server according to flask template
        # temporary solution:
        end_results = self.server.aggregate_results(chunk_results, self.SNP_names)

    ########################### major functions ##############################
    def initialize(self, bed_path, fam_path, cov_path, bim_path, confounding_features):

        # TODO: add other algorithms after implementing them
        if self.algorithm not in ["Chi-square", "logreg"]:
            print("Error: the specified algorithm: " + self.algorithm + " is not implemented.\n"
                                                                        "Choose one of: 'Chi-square'.")
        self.open_files(bed_path, fam_path, cov_path, bim_path)
        # open_files() also creates the phenotype_vector (Y)
        self.create_feature_matrix(self.byte_list, self.cov_file, confounding_features, self.sample_IIDs)

    # opens all files and creates the phenotype vector (Y)
    def open_files(self, bed_path, fam_path, cov_path, bim_path):

        self.open_fam(fam_path)
        self.cov_file = open(cov_path, "r")
        # open_bed() has to be called after open_fam(), since open_bed() calls get_sample_count().
        self.open_bed(bed_path)
        self.bim_file = open(bim_path, "r")

    # reads SNP names, first alleles and second alleles
    def process_bim(self, file):

        try:
            for line in file.readlines():
                cols = line.rstrip("\n").split("\t")
                self.SNP_names.append(cols[1])
                self.first_alleles.append(cols[4])
                self.second_alleles.append(cols[5])
        except Exception as exception:
            print("Exception: no proper .bim file!")
            return

    # creates the vector chunk_starting_points according to the input
    def split_SNPs_into_chunks(self, SNPs, number_of_chunks):

        # change vector to sth else than [0] only if required
        if number_of_chunks <= 1:
            return
        else:
            size_chunks = math.ceil(len(SNPs) / number_of_chunks)
            for i in range(number_of_chunks - 1):
                self.chunk_starting_points.append((i + 1) * size_chunks)

    # returns the local allele counts of a chunk starting at the given point
    def calc_local_allele_count(self, chunk_no):

        # define start and end of the chunk; start inclusive, end exclusive
        starting_point, end_point = self.get_chunk_limiters(chunk_no)

        # get data
        snp_chunk = self.SNP_values[starting_point:end_point]

        # start counting
        first_allele_count = []
        second_allele_count = []
        for snp in range(0, end_point - starting_point):
            first_allele_count.append(2 * snp_chunk[snp].count(0) + snp_chunk[snp].count(2))
            second_allele_count.append(2 * snp_chunk[snp].count(3) + snp_chunk[snp].count(2))

        return first_allele_count, second_allele_count, chunk_no

    # reformat of parallel allele counts so that it can be used as server input
    def get_allele_count_format_for_server(self, parallel_counts):

        first_allele_count = []
        second_allele_count = []

        for chunk in range(len(parallel_counts)):
            first_allele_count += parallel_counts[chunk][0]
            second_allele_count += parallel_counts[chunk][1]

        # put everything together
        return self.SNP_names, self.first_alleles, self.second_alleles, first_allele_count, second_allele_count

    # uses the vector of minor alleles and returns the contingency tables of the given chunk of SNPs
    def swap_values_and_start_algorithm(self, chunk_no):
        # define start and end point of the chunk; start inclusive, end exclusive
        starting_point, end_point = self.get_chunk_limiters(chunk_no)

        # get data
        chunk_snp_values = self.SNP_values[starting_point:end_point]
        control_indices = [i for i in range(len(self.phenotype_vector)) if self.phenotype_vector[i] == '1']
        case_indices = [i for i in range(len(self.phenotype_vector)) if self.phenotype_vector[i] == '2']

        # swap genotype values according to global_minor_allele_names
        # the server returns a list of SNP names and their global minor allele name.
        for SNP in range(starting_point, end_point):
            # if global minor allele name != local minor allele name
            if self.global_minor_allele_names[self.SNP_names[SNP]] != self.first_alleles[SNP]:
                # swap values 0 and 3 (not 0 and 2 as in the paper!) of the SNP_values
                for value in range(len(self.phenotype_vector)):
                    if chunk_snp_values[SNP - starting_point][value] == 0:
                        chunk_snp_values[SNP - starting_point][value] = 3
                    elif chunk_snp_values[SNP - starting_point][value] == 3:
                        chunk_snp_values[SNP - starting_point][value] = 0

        # start selected algorithm
        if self.algorithm == "Chi-square":
            results = self.chi_square(end_point, starting_point, chunk_snp_values, control_indices, case_indices)
        # TODO: implement other algorithms here
        elif self.algorithm == "logreg":
            results = self.beta_step(self.beta, starting_point, end_point, chunk_snp_values)

        else:
            print("Error: the specified algorithm: " + self.algorithm + " is not implemented.\n"
                                                                        "Choose one of: 'Chi-square'.")
            return

        return results

    ######################### algorithms #####################################

    # this is the implementation of the Chi-square test
    def chi_square(self, end_point, starting_point, chunk_snp_values, control_indices, case_indices):
        tables = []
        for snp in range(0, end_point - starting_point):

            p = 0
            q = 0
            r = 0
            s = 0

            for index in case_indices:
                # p: minor allele (always first allele after swapping) in cases
                # q: major allele (always second allele after swapping) in cases
                if chunk_snp_values[snp][index] == 0:
                    p += 2
                elif chunk_snp_values[snp][index] == 3:
                    q += 2
                elif chunk_snp_values[snp][index] == 2:
                    p += 1
                    q += 1

            for index in control_indices:
                # r: minor allele in controls
                # s: major allele in controls
                if chunk_snp_values[snp][index] == 0:
                    r += 2
                elif chunk_snp_values[snp][index] == 3:
                    s += 2
                elif chunk_snp_values[snp][index] == 2:
                    r += 1
                    s += 1

            tables.append([p, q, r, s])

        return tables

    # Begin of logistic Regression
    def beta_step(self, beta, starting_point, end_point, chunk_SNP_values):
        gradient = {}
        hessian = {}
        log_likelihood = {}
        # Get phenotype values for the Y-Vektor
        phenotype = self.phenotype_vector
        phenotype = np.array([int(value) for value in phenotype])
        for index in range(0, end_point - starting_point):
            SNP_name = self.SNP_names[index + starting_point]
            # Create X-Matrix from genotype values and feature values
            tmp = np.array(chunk_SNP_values[index]).reshape(-1, 1)
            missing_genotype = np.where(tmp == 1)[0]
            ones = np.where(tmp == 2)[0]
            twos = np.where(tmp == 3)[0]
            tmp[missing_genotype] = 3
            tmp[ones] = 1
            tmp[twos] = 2
            non_missing_indices = np.where(phenotype != -9)[0]
            non_missing_indices = np.intersect1d(non_missing_indices, np.where(tmp != 3)[0])
            y = np.transpose(phenotype[non_missing_indices][np.newaxis])
            x = np.ones((len(non_missing_indices), 1)).reshape(-1, 1).astype(np.uint8)
            x = np.concatenate((x, tmp[non_missing_indices]), axis=1)
            for feature in self.feature_values:
                tmp = np.array(feature)[non_missing_indices].reshape(-1, 1)
                x = np.concatenate((x, tmp), axis=1)
            x = x.astype(np.float32)
            y2 = self.sigmoid_function(np.matmul(x, self.beta[SNP_name]))
            gradient[SNP_name] = self.gradient(x, y, y2)
            hessian[SNP_name] = self.hessian(x, y2)
            log_likelihood[SNP_name] = self.log_likelihood(y, y2)

        # apply beta and calculate gradient, hessian and log-likelihood
        results = {"gradient": gradient, "hessian": hessian, "log_likelihood": log_likelihood}
        return results

    def gradient(self, x, y, y2):
        return np.matmul(np.transpose(x), (y - y2))

    def hessian(self, x, y2):
        return np.matmul((np.transpose(x) * np.transpose(y2 * (1 - y2))), x)

    def log_likelihood(self, y, y2):
        return np.sum(y * np.log(y2) + (1 - y) * np.log(1 - y2))

    def sigmoid_function(self, x_beta):
        return 1 / (1 + np.exp(-x_beta))

    ########################### helper functions #############################

    # opens and processes the .fam file.
    # creates the phenotype_vector Y
    def open_fam(self, path):

        try:
            self.fam_file = open(path, "r")
            # for now we will only need the phenotype vector
            # append the last column of every line (control/case as '1'/'2'; separated by space) to the phenotype vector
            for line in self.fam_file.readlines():
                split = line.rstrip('\n').split(' ')
                self.phenotype_vector.append(split[len(split) - 1])
                self.sample_IIDs.append(split[1])

        except Exception as exception:
            print("Exception in open_fam()")
            self.fam_file.close()
            return

    # opens the .bed file, which is a binary
    # adapted from sPLINK source code
    def open_bed(self, path):
        try:
            bed_file = open(path, "rb")
            first_byte, second_byte, third_byte = bed_file.read(3)

            MAGIC_BYTE_1 = int('01101100', 2)
            MAGIC_BYTE_2 = int('00011011', 2)

            if not (first_byte == MAGIC_BYTE_1 and second_byte == MAGIC_BYTE_2):
                # self.operation_status = OperationStatus.FAILED
                # self.log(file_path + "is not a proper bed file!")
                print("Not a proper bed file selected!")
                return

            if third_byte != 1:
                # self.operation_status = OperationStatus.FAILED
                # self.log("bed file must be snp-major!")
                print("bed file must be SNP-major!")
                return

            self.bed_file = bed_file

            self.bed_file.seek(3)
            self.byte_list = np.fromfile(self.bed_file, dtype=np.uint8)
            self.per_SNP_byte_count = math.ceil(self.get_sample_count() / 4)

        except Exception as exception:
            # self.log(f"{exception}")
            # self.operation_status = OperationStatus.FAILED
            self.bed_file.close()
            print("Exception in open_bed() !")
            return

    # returns the number of samples from .fam
    def get_sample_count(self):
        return len(self.phenotype_vector)

    # uses the .bed and .cov file as well as the confounding features list to create the feature matrix (X)
    # This matrix is split up into the matrix of SNPs against all samples and the matrix of confounding features
    # against all samples.
    def create_feature_matrix(self, byte_list, cov_file, confounding_features, sample_IIDs):
        # structure: 1 matrix of all SNPs vs all samples,
        #            x additional vectors of the confounding features

        # The genotype of all samples for 1 SNP is encoded in per_SNP_byte_count bytes.
        # There are 4 values in 1 byte, meaning 2 bits encode 1 genotype value.
        # Every per_SNP_byte_count'th byte there are less values encoded, if the number of samples % 4 != 0

        # initialize 2 dimensional SNP array
        self.SNP_values = [[0 for i in range(self.get_sample_count())]
                           for j in range(int(len(byte_list) / self.per_SNP_byte_count))]
        SNP_count = 0
        byte_count = 0
        bits = ""
        for byte in byte_list:

            # concatenate all bits until one SNP is done
            new_bits = bin(byte).replace("0b", "")

            # do not neglect leading 0s
            new_bits = (8 - len(new_bits)) * "0" + new_bits

            # shorten last byte of SNP accordingly
            if byte_count == self.per_SNP_byte_count - 1:
                new_bits = new_bits[- (self.get_sample_count() % 4) * 2:]

            bits = bits + new_bits
            byte_count += 1

            if byte_count == self.per_SNP_byte_count:

                # translate these bits to genotype data for one SNP
                for bit in range(int(len(bits) / 2)):
                    genotype = bits[2 * bit: 2 * bit + 2]

                    self.SNP_values[SNP_count][bit] = self.bin_genotype_to_dec[genotype]

                SNP_count += 1
                byte_count = 0
                bits = ""

        # confounding feature vectors
        cov_header = cov_file.readline().rstrip("\n").split(" ")
        cov_col_indices = []

        for feature in confounding_features:
            if feature not in cov_header:
                print('Exception: Confounding feature "' + feature + '" not found in .cov file.')
                return
            else:
                cov_col_indices.append(cov_header.index(feature))

        # Match IID (second column) in .cov file with IIDs in .fam file, since .cov does not have the right number of samples
        cov_dictionary = {}
        for line in cov_file.readlines():
            split = line.rstrip("\n").split(" ")
            cov_dictionary[split[1]] = [split[i] for i in cov_col_indices]

        self.feature_values = [[0 for i in range(self.get_sample_count())]
                               for j in range(len(confounding_features))]

        for iid in range(len(sample_IIDs)):
            for feature in range(len(confounding_features)):
                self.feature_values[feature][iid] = cov_dictionary[sample_IIDs[iid]][feature]

    # returns starting_point and end_point for a chunk; for the parallel functions
    def get_chunk_limiters(self, chunk_no):
        # define start and end point of this chunk
        starting_point = self.chunk_starting_points[chunk_no]  # inclusive
        if chunk_no == len(self.chunk_starting_points) - 1:
            end_point = len(self.SNP_names)  # exclusive
        else:
            end_point = self.chunk_starting_points[chunk_no + 1]  # exclusive
        return starting_point, end_point


class Server:

    def __init__(self, confounding_features, algorithm):
        self.beta = {}
        self.likelihoods = {}
        self.gradients = {}
        self.hessians = {}
        self.confounding_features = confounding_features
        self.algorithm = algorithm

        self.allele_counts = []

        self.end_result = []

    ############### server methods ##########################
    # TODO: change server methods accordingly to flask template later on
    # TODO: implement test, whether all clients completed their steps successfully

    # returns a dictionary with the minor allele names of every contributed SNP
    def get_global_minor_alleles(self, input):

        # simulate several clients by duplicating the input
        # TODO: change this to a real federated input later on
        for i in range(2):
            self.allele_counts.append(input)

        local_counts = self.allele_counts

        # go through all sent allele counts of all clients
        alleles = {}
        for client in range(len(local_counts)):
            for SNP in range(len(local_counts[client][0])):

                # add local allele count to global allele count, match by allele names
                if local_counts[client][0][SNP] not in alleles:
                    # new entry in dictionary
                    alleles[local_counts[client][0][SNP]] = [local_counts[client][1][SNP],
                                                             local_counts[client][2][SNP],
                                                             local_counts[client][3][SNP],
                                                             local_counts[client][4][SNP]]
                else:
                    # check for allele names:
                    if alleles[local_counts[client][0][SNP]][0] == local_counts[client][1][SNP]:
                        # the minor allele name is equal
                        alleles[local_counts[client][0][SNP]][2] += local_counts[client][3][SNP]
                        alleles[local_counts[client][0][SNP]][3] += local_counts[client][4][SNP]
                    else:
                        # the minor allele name not equal
                        alleles[local_counts[client][0][SNP]][2] += local_counts[client][4][SNP]
                        alleles[local_counts[client][0][SNP]][3] += local_counts[client][3][SNP]

        # replace global count array by minor allele names
        for SNP in alleles:
            if alleles[SNP][2] <= alleles[SNP][3]:
                alleles[SNP] = alleles[SNP][0]
            else:
                alleles[SNP] = alleles[SNP][1]

        # return dictionary of SNPs and their minor allele name
        return alleles

    # delegates client results to the specified algorithm for global computation
    def aggregate_results(self, client_results, SNP_names):
        if self.algorithm == "Chi-square":
            self.end_result = self.aggregate_chi_square(client_results, SNP_names)

        # TODO: implement the other algorithms here
        elif self.algorithm == "logreg":
            if len(self.beta) == 0:
                length = len(client_results[0]["gradient"][SNP_names[0]])
                for SNP_name in SNP_names:
                    self.beta[SNP_name] = np.zeros((length, 1)).astype(np.float)
            pool = mp.Pool(len(client_results))
            self.end_result = pool.map(self.aggregate_beta, client_results)
            print("New betas:")
            print(self.end_result)

        else:
            print("Error: the specified algorithm: " + self.algorithm + " is not implemented.\n"
                                                                        "Choose one of: 'Chi-square'.")

    def aggregate_chi_square(self, local_tables, SNP_names):

        client_tables = []
        client_SNP_names = []

        # again, simulate several clients
        # TODO: change this to a real federated input later on
        client_tables.append(local_tables)
        client_tables.append(local_tables)
        client_SNP_names.append(SNP_names)
        client_SNP_names.append(SNP_names)

        # find subset of SNP_names that are included in every client's survey
        # add every SNP of the first client
        common_SNP_names = client_SNP_names[0]
        new_common_SNP_names = []
        global_observed_contingency_tables = {}

        # remove the SNP names that do not occur at some point in the other clients
        for client_no in range(1, len(client_SNP_names)):
            for common_name in common_SNP_names:
                if common_name in client_SNP_names[client_no]:
                    new_common_SNP_names.append(common_name)
                    if client_no == len(client_SNP_names) - 1:
                        # set up dictionary global_observed_contingency_tables
                        global_observed_contingency_tables[common_name] = [0, 0, 0, 0]
            common_SNP_names = new_common_SNP_names

        # match the clients' contingency tables with the common_SNP_names
        # calculate the global observed contingency tables O: [p, q, r, s]
        snp_no_in_client = 0
        for client_no in range(len(client_SNP_names)):
            for chunk_no in range(len(client_tables[client_no])):
                for snp_no in range(len(client_tables[client_no][chunk_no])):
                    if client_SNP_names[client_no][snp_no_in_client] in global_observed_contingency_tables:
                        # add p,q,r,s values of local contingency table to global one
                        global_observed_contingency_tables[client_SNP_names[client_no][snp_no_in_client]] = \
                            list(map(add,
                                     global_observed_contingency_tables[client_SNP_names[client_no][snp_no_in_client]],
                                     client_tables[client_no][chunk_no][snp_no]))

                    snp_no_in_client += 1

            snp_no_in_client = 0

        print("O: ", global_observed_contingency_tables)

        # calculate global expected contingency table E: [p, q, r, s],
        # odds ration OR,
        # Chi-square
        # and p-values
        global_expected_contingency_tables = {}
        odds_ratio = {}
        chi_square = {}
        p_values = {}
        for snp in global_observed_contingency_tables:

            p = global_observed_contingency_tables[snp][0]
            q = global_observed_contingency_tables[snp][1]
            r = global_observed_contingency_tables[snp][2]
            s = global_observed_contingency_tables[snp][3]
            n = p + q + r + s

            # expected value = (rowsum * colsum) / all
            p1 = (p + q) * (p + r) / n
            q1 = (q + p) * (q + s) / n
            r1 = (r + s) * (r + p) / n
            s1 = (s + r) * (s + q) / n

            global_expected_contingency_tables[snp] = [p1, q1, r1, s1]

            try:
                odds_ratio[snp] = (p * s) / (q * r)
            # TODO: what to return if OR is not defined?
            except ZeroDivisionError as exception:
                print(snp, p, q, r, s, ": q or r are 0. Odds-ratio is not defined")

            chi_square_sum = 0
            for i in range(0, 4):
                chi_square_sum += pow(global_expected_contingency_tables[snp][i] -
                                      global_observed_contingency_tables[snp][i], 2) \
                                  / global_expected_contingency_tables[snp][i]
            chi_square[snp] = chi_square_sum

            p_values[snp] = 1 - chi2.cdf(chi_square_sum, 1)

        print("E: ", global_expected_contingency_tables)
        print("OR: ", odds_ratio)
        print("Chi-square: ", chi_square)
        print("p-values: ", p_values)

        return (odds_ratio, chi_square, p_values)

    def aggregate_beta(self, client_results):
        beta = {}
        hess = {}
        grad = {}
        log = {}
        for SNP_Name in client_results["gradient"]:
            hess[SNP_Name] = client_results["hessian"][SNP_Name]
            grad[SNP_Name] = client_results["gradient"][SNP_Name]
            log[SNP_Name] = client_results["log_likelihood"][SNP_Name]
            beta[SNP_Name] = self.log_beta_calc(self.beta[SNP_Name], hess[SNP_Name], grad[SNP_Name])
        return beta

    # Beta calculation for logistic regression
    def log_beta_calc(self, old_beta, hessian, gradient):
        return old_beta + np.matmul(np.linalg.inv(hessian), gradient)

    # Calculation of Standarderror and T statistics
    def log_se(self, global_hessian):
        return np.roots(np.diag(np.linalg.inv(global_hessian)))

    def log_T(self, beta, se):
        return beta / se


if __name__ == '__main__':
    # test
    client = Client(Server("Age,Sex", "logreg"), "../../toy_logistic_split1.bed",
                    "../../toy_logistic_split1_altered.fam", "../../toy_logistic_split1.cov",
                    "../../toy_logistic_split1.bim", "Age,Sex", 6, "logreg")

