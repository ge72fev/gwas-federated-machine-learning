##!/usr/bin/python3

# This code implements the methods of both server and client in federated GWAS as to be used by FeatureCloud.
# This is only the algorithm to be performed and tested locally.
# Parts of this code are adapted from the official sPLINK source code.

# imports needed by client
import numpy as np
import math
import pathos.multiprocessing as mp     # absolutely use pathos. Otherwise the multiprocessing will not work on Windows.

# import needed by server
from operator import add
from scipy.stats import chi2
from pandas.api.types import is_integer_dtype
from scipy import stats
from utils import *

class Client:

    def __init__(self, server, bed_path, fam_path, cov_path, bim_path, confounding_features, number_of_chunks, algorithm):

        self.bed_path = bed_path
        self.fam_path = fam_path
        self.cov_path = cov_path
        self.bim_path = bim_path

        self.confounding_features = confounding_features.split(",")
        self.number_of_chunks = number_of_chunks
        self.algorithm = algorithm

        # initialize attributes that will be used later on
        self.bed_file = None
        self.fam_file = None
        self.cov_file = None
        self.bim_file = None

        self.phenotype_vector = []
        self.sample_IIDs = []
        self.SNP_values = []
        self.feature_values = [[]]

        self.SNP_names = []
        self.first_alleles = []
        self.second_alleles = []
        self.global_minor_allele_names = {}

        # here, each number in the list refers to the starting point of a new chunk.
        # the list is created in split_SNPs_into_chunks()
        self.chunk_starting_points = [0]

        # attributes needed for reading the .bed file
        self.byte_list = []
        self.per_SNP_byte_count = 0

        self.bin_genotype_to_dec = {"00": 2,  # homozygous, 1. allele
                                    "01": 3,  # missing value
                                    "10": 1,  # heterozygous
                                    "11": 0   # homozygous, 2. allele
        }



        # 1. Initialize
        self.server = server
        self.initialize(self.bed_path, self.fam_path, self.cov_path, self.bim_path, self.confounding_features)

        # 2. extract data from .bim file
        self.process_bim(self.bim_file)

        # close all files
        self.bed_file.close()
        self.fam_file.close()
        self.cov_file.close()
        self.bim_file.close()

        # 3. calculate local sample count (n)
        self.local_sample_count = len(self.phenotype_vector)

        # calculate chunks
        self.split_SNPs_into_chunks(self.SNP_names, self.number_of_chunks)

        # 4.1 calculate non-missing sample count
        # a '-9' in the phenotype vector encodes a missing sample
        self.non_missing_sample_count = self.get_sample_count() - self.phenotype_vector.count('-9')

        # 4.2 start parallelization & allele count
        # will be most effective if number_of_chunks == number of processors in your pc
        # to check this, run: print(mp.cpu_count()) (after importing multiprocessing as mp)
        #
        # simple test to check if parallelization works in general:
        #
        # from math import cos
        # p = mp.Pool(2)
        # results = p.map(cos, range(10))
        # print(results)

        pool = mp.Pool(self.number_of_chunks)

        # parallel step 1: allele count
        parallel_allele_counts = pool.map(self.calc_local_allele_count,  range(self.number_of_chunks))
        # parallel allele counts has the values (first_allele_vector, second_allele_vector, chunk_no) for each chunk

        # 5. Minor alleles
        # reformat allele counts and add their names to send to the server
        server_input_allele_counts = self.get_allele_count_format_for_server(parallel_allele_counts)
        # server_input_allele_counts lists SNP_names, first_allele_names, second_allele_names, first_allele_counts and
        # second allele counts as separate fields one after another.


        # send allele counts to server
        # temporary solution:
        self.global_minor_allele_names = self.server.get_global_minor_alleles(server_input_allele_counts)


        # 5. - 6. process global minor alleles and create contingency tables, parallel step 2
        chunk_results = pool.map(self.swap_values_and_start_algorithm, range(self.number_of_chunks))
        pool.close()

        XT_X_matrix, XT_Y_vector = self.beta_step()
        #print(XT_X_matrix)
        #print(XT_Y_vector)
        beta = self.server.beta_step_aggregation(XT_X_matrix, XT_Y_vector)
        real_predicted_Y_sum_square_error = self.std_error_step(beta)
        self.server.std_error_step_aggregation(real_predicted_Y_sum_square_error, self.non_missing_sample_count)
        self.server.constructLinRegOutput()



    ########################### major functions ##############################
    def initialize(self, bed_path, fam_path, cov_path, bim_path, confounding_features):

        # TODO: add other algorithms after implementing them
        if self.algorithm not in ["Lin_Reg"]:
            print("Error: the specified algorithm: " + self.algorithm + " is not implemented.\n"
                                                                        "Choose one of: 'Lin_Reg'.")
        self.open_files(bed_path, fam_path, cov_path, bim_path)
        # open_files() also creates the phenotype_vector (Y)
        self.create_feature_matrix(self.byte_list, self.cov_file, confounding_features, self.sample_IIDs)

    # opens all files and creates the phenotype vector (Y)
    def open_files(self, bed_path, fam_path, cov_path, bim_path):

        self.open_fam(fam_path)
        self.cov_file = open(cov_path, "r")
        # open_bed() has to be called after open_fam(), since open_bed() calls get_sample_count().
        self.open_bed(bed_path)
        self.bim_file = open(bim_path, "r")

    # reads SNP names, first alleles and second alleles
    def process_bim(self, file):

        try:
            for line in file.readlines():
                cols = line.rstrip("\n").split("\t")
                self.SNP_names.append(cols[1])
                self.first_alleles.append(cols[4])
                self.second_alleles.append(cols[5])
        except Exception as exception:
            print("Exception: no proper .bim file!")
            return

    # creates the vector chunk_starting_points according to the input
    def split_SNPs_into_chunks(self, SNPs, number_of_chunks):

        # change vector to sth else than [0] only if required
        if number_of_chunks <= 1:
            return
        else:
            size_chunks = math.ceil(len(SNPs) / number_of_chunks)
            for i in range(number_of_chunks - 1):
                self.chunk_starting_points.append((i + 1) * size_chunks)

    # returns the local allele counts of a chunk starting at the given point
    def calc_local_allele_count(self, chunk_no):

        # define start and end of the chunk; start inclusive, end exclusive
        starting_point, end_point = self.get_chunk_limiters(chunk_no)

        # get data
        snp_chunk = self.SNP_values[starting_point:end_point]

        # start counting
        first_allele_count = []
        second_allele_count = []
        for snp in range(0, end_point - starting_point):

            first_allele_count.append(2 * snp_chunk[snp].count(0) + snp_chunk[snp].count(2))
            second_allele_count.append(2 * snp_chunk[snp].count(3) + snp_chunk[snp].count(2))

        return first_allele_count, second_allele_count, chunk_no

    # reformat of parallel allele counts so that it can be used as server input
    def get_allele_count_format_for_server(self, parallel_counts):

        first_allele_count = []
        second_allele_count = []

        for chunk in range(len(parallel_counts)):
            first_allele_count += parallel_counts[chunk][0]
            second_allele_count += parallel_counts[chunk][1]

        # put everything together
        return self.SNP_names, self.first_alleles, self.second_alleles, first_allele_count, second_allele_count

    # uses the vector of minor alleles and returns the contingency tables of the given chunk of SNPs
    def swap_values_and_start_algorithm(self, chunk_no):

        # define start and end point of the chunk; start inclusive, end exclusive
        starting_point, end_point = self.get_chunk_limiters(chunk_no)

        # get data
        chunk_snp_values = self.SNP_values[starting_point:end_point]
        control_indices = [i for i in range(len(self.phenotype_vector)) if self.phenotype_vector[i] == '1']
        case_indices = [i for i in range(len(self.phenotype_vector)) if self.phenotype_vector[i] == '2']

        # swap genotype values according to global_minor_allele_names
        # the server returns a list of SNP names and their global minor allele name.
        for SNP in range(starting_point, end_point):
            # if global minor allele name != local minor allele name
            if self.global_minor_allele_names[self.SNP_names[SNP]] != self.first_alleles[SNP]:
                # swap values 0 and 3 (not 0 and 2 as in the paper!) of the SNP_values
                for value in range(len(self.phenotype_vector)):
                    if chunk_snp_values[SNP - starting_point][value] == 0:
                        chunk_snp_values[SNP - starting_point][value] = 3
                    elif chunk_snp_values[SNP - starting_point][value] == 3:
                        chunk_snp_values[SNP - starting_point][value] = 0


        # start selected algorithm in original version


    ######################### algorithms #####################################

    #sPLINK client beta step
    def beta_step(self):
        self.Y_vector, self.X_matrix = self.get_Y_vector_X_matrix()
        #print(self.X_matrix)
        #print(self.Y_vector)
        XT_X_matrix = {}
        XT_Y_vector = {}
        self.non_missing_sample_count = {}
        for SNP_ID in self.X_matrix.keys():
            XT_X_matrix[SNP_ID] = np.dot(self.X_matrix[SNP_ID].T, self.X_matrix[SNP_ID])
            XT_Y_vector[SNP_ID] = np.dot(self.X_matrix[SNP_ID].T, self.Y_vector[SNP_ID])
            self.non_missing_sample_count[SNP_ID] = len(self.Y_vector[SNP_ID])

        #print(self.non_missing_sample_count)
        return XT_X_matrix, XT_Y_vector

    #sPLINK client stdErr step
    def std_error_step(self, beta):
        real_predicted_Y_sum_square_error = {}
        for SNP_ID in self.X_matrix.keys():
            beta_vector = np.array(beta[SNP_ID]).reshape(-1, 1)
            predicted_Y = np.dot(self.X_matrix[SNP_ID], beta_vector)
            #print(predicted_Y)
            real_predicted_Y_sum_square_error[SNP_ID] = np.sum(np.square(self.Y_vector[SNP_ID] - predicted_Y))
        return real_predicted_Y_sum_square_error

    #generate propper xMatrices and yVectors for regression
    def get_Y_vector_X_matrix(self):
        try:
            yVectors = {}
            xMatrices = {}
            y = self.phenotype_vector
            y = np.array([int(value) for value in y])
            non_missing_indices = np.where(y != -9)[0]
            for i in range(len(self.SNP_names)):
                # Get phenotype values for the Y-Vektor
                tmp = np.array(self.SNP_values[i])[non_missing_indices]
                non_missing_indices_2 = np.where(tmp != 3)[0]
                y2 = np.transpose(y[non_missing_indices][non_missing_indices_2][np.newaxis])
                yVectors[self.SNP_names[i]] = y2

                # Create X-Matrix from genotype values and feature values
                x = np.ones((len(non_missing_indices_2), 1)).reshape(-1, 1).astype(np.uint8)
                #for SNP_list in self.SNP_values:
                #tmp = np.array(SNP_list)[non_missing_indices].reshape(-1, 1)
                tmp = np.array(self.SNP_values[i])[non_missing_indices][non_missing_indices_2].reshape(-1, 1)
                x = np.concatenate((x, tmp), axis=1)
                for feature in self.feature_values:
                    tmp = np.array(feature)[non_missing_indices][non_missing_indices_2].reshape(-1, 1)
                    x = np.concatenate((x, tmp), axis=1)
                x = x.astype(np.float32)
                xMatrices[self.SNP_names[i]] = x

            return yVectors, xMatrices

            #genotype_index_list_non_missing = \
            #np.where(self.genotype_list[genotype_name] != ConstantParameters.GENOTYPE_MISSING_VALUE)[0]

            #non_missing_index_list = np.intersect1d(self.non_missing_index_list, genotype_index_list_non_missing)

            #Y_vector = self.phenotype_list[non_missing_index_list].reshape(-1, 1)
            #if is_integer_dtype(Y_vector):
            #    Y_vector = Y_vector.astype(np.uint8)

            #genotype_vector = self.genotype_list[genotype_name][non_missing_index_list].reshape(-1, 1).astype(np.uint8)
            #feature_matrix = np.array(genotype_vector)

            #for feature_name in self.extra_feature_list:
            #    feature_value_vector = self.get_extra_feature_value_list(feature_name)[non_missing_index_list].reshape(-1, 1)

            #    feature_matrix = np.concatenate((feature_matrix, feature_value_vector), axis=1)

            #non_missing_sample_count = len(non_missing_index_list)
            #column_one = np.ones((non_missing_sample_count, 1)).astype(np.uint8)
            #X_matrix = np.concatenate((column_one, feature_matrix), axis=1)

            #return Y_vector, X_matrix

        except Exception as exception:
            print(exception)
            print("Error in Xm/Yv creation")
            #self.log(f'{exception}')
            #self.operation_status = OperationStatus.FAILED

    ########################### helper functions #############################

    # opens and processes the .fam file.
    # creates the phenotype_vector Y
    def open_fam(self, path):

        try:
            self.fam_file = open(path, "r")
            # for now we will only need the phenotype vector
            # append the last column of every line (control/case as '1'/'2'; separated by space) to the phenotype vector
            for line in self.fam_file.readlines():
                split = line.rstrip('\n').split(' ')
                self.phenotype_vector.append(split[len(split)-1])
                self.sample_IIDs.append(split[1])

        except Exception as exception:
            print("Exception in open_fam()")
            self.fam_file.close()
            return

    # opens the .bed file, which is a binary
    # adapted from sPLINK source code
    def open_bed(self, path):
        try:
            bed_file = open(path, "rb")
            first_byte, second_byte, third_byte = bed_file.read(3)

            MAGIC_BYTE_1 = int('01101100', 2)
            MAGIC_BYTE_2 = int('00011011', 2)

            if not (first_byte == MAGIC_BYTE_1 and second_byte == MAGIC_BYTE_2):
                #self.operation_status = OperationStatus.FAILED
                #self.log(file_path + "is not a proper bed file!")
                print("Not a proper bed file selected!")
                return

            if third_byte != 1:
                #self.operation_status = OperationStatus.FAILED
                #self.log("bed file must be snp-major!")
                print("bed file must be SNP-major!")
                return

            self.bed_file = bed_file

            self.bed_file.seek(3)
            self.byte_list = np.fromfile(self.bed_file, dtype=np.uint8)
            self.per_SNP_byte_count = math.ceil(self.get_sample_count() / 4)

        except Exception as exception:
            #self.log(f"{exception}")
            #self.operation_status = OperationStatus.FAILED
            self.bed_file.close()
            print("Exception in open_bed() !")
            return

    # returns the number of samples from .fam
    def get_sample_count(self):
        return len(self.phenotype_vector)

    # uses the .bed and .cov file as well as the confounding features list to create the feature matrix (X)
    # This matrix is split up into the matrix of SNPs against all samples and the matrix of confounding features
    # against all samples.
    def create_feature_matrix(self, byte_list, cov_file, confounding_features, sample_IIDs):
        # structure: 1 matrix of all SNPs vs all samples,
        #            x additional vectors of the confounding features

        # The genotype of all samples for 1 SNP is encoded in per_SNP_byte_count bytes.
        # There are 4 values in 1 byte, meaning 2 bits encode 1 genotype value.
        # Every per_SNP_byte_count'th byte there are less values encoded, if the number of samples % 4 != 0

        #initialize 2 dimensional SNP array
        self.SNP_values = [[0 for i in range(self.get_sample_count())]
                           for j in range(int(len(byte_list) / self.per_SNP_byte_count))]
        SNP_count = 0
        byte_count = 0
        bits = ""
        for byte in byte_list:

            # concatenate all bits until one SNP is done
            new_bits = bin(byte).replace("0b", "")

            # do not neglect leading 0s
            new_bits = (8 - len(new_bits)) * "0" + new_bits

            # shorten last byte of SNP accordingly
            if byte_count == self.per_SNP_byte_count - 1:
                new_bits = new_bits[- (self.get_sample_count() % 4) * 2:]

            bits = bits + new_bits
            byte_count += 1

            if byte_count == self.per_SNP_byte_count:

                # translate these bits to genotype data for one SNP
                for bit in range(int(len(bits) / 2)):
                    genotype = bits[2 * bit: 2 * bit + 2]

                    self.SNP_values[SNP_count][bit] = self.bin_genotype_to_dec[genotype]

                SNP_count += 1
                byte_count = 0
                bits = ""

        # confounding feature vectors
        cov_header = cov_file.readline().rstrip("\n").split(" ")
        cov_col_indices = []

        for feature in confounding_features:
            if feature not in cov_header:
                print('Exception: Confounding feature "' + feature + '" not found in .cov file.')
                return
            else:
                cov_col_indices.append(cov_header.index(feature))

        # Match IID (second column) in .cov file with IIDs in .fam file, since .cov does not have the right number of samples
        cov_dictionary = {}
        for line in cov_file.readlines():
            split = line.rstrip("\n").split(" ")
            cov_dictionary[split[1]] = [split[i] for i in cov_col_indices]

        self.feature_values = [[0 for i in range(self.get_sample_count())]
                               for j in range(len(confounding_features))]

        for iid in range(len(sample_IIDs)):
            for feature in range(len(confounding_features)):
                self.feature_values[feature][iid] = cov_dictionary[sample_IIDs[iid]][feature]

    # returns starting_point and end_point for a chunk; for the parallel functions
    def get_chunk_limiters(self, chunk_no):
        # define start and end point of this chunk
        starting_point = self.chunk_starting_points[chunk_no]  # inclusive
        if chunk_no == len(self.chunk_starting_points) - 1:
            end_point = len(self.SNP_names)  # exclusive
        else:
            end_point = self.chunk_starting_points[chunk_no + 1]  # exclusive
        return starting_point, end_point



class Server:


    def __init__(self, confounding_features, algorithm):

        self.confounding_features = confounding_features
        self.algorithm = algorithm

        self.allele_counts = []

        self.end_result = []


    ############### server methods ##########################

    # returns a dictionary with the minor allele names of every contributed SNP
    def get_global_minor_alleles(self, input):

        # simulate several clients by duplicating the input
        for i in range(2):
            self.allele_counts.append(input)

        local_counts = self.allele_counts

        # go through all sent allele counts of all clients
        alleles = {}
        for client in range(len(local_counts)):
            for SNP in range(len(local_counts[client][0])):

                # add local allele count to global allele count, match by allele names
                if local_counts[client][0][SNP] not in alleles:
                    # new entry in dictionary
                    alleles[local_counts[client][0][SNP]] = [local_counts[client][1][SNP],
                                                             local_counts[client][2][SNP],
                                                             local_counts[client][3][SNP],
                                                             local_counts[client][4][SNP]]
                else:
                    # check for allele names:
                    if alleles[local_counts[client][0][SNP]][0] == local_counts[client][1][SNP]:
                        # the minor allele name is equal
                        alleles[local_counts[client][0][SNP]][2] += local_counts[client][3][SNP]
                        alleles[local_counts[client][0][SNP]][3] += local_counts[client][4][SNP]
                    else:
                        # the minor allele name not equal
                        alleles[local_counts[client][0][SNP]][2] += local_counts[client][4][SNP]
                        alleles[local_counts[client][0][SNP]][3] += local_counts[client][3][SNP]

        # replace global count array by minor allele names
        for SNP in alleles:
            if alleles[SNP][2] <= alleles[SNP][3]:
                alleles[SNP] = alleles[SNP][0]
            else:
                alleles[SNP] = alleles[SNP][1]

        # return dictionary of SNPs and their minor allele name
        return alleles

    # delegates client results to the specified algorithm for global computation


    #sPLINK beta aggregation step
    def beta_step_aggregation(self, client_XT_X_matrix, client_XT_Y_vector):
        #print("Aggregation of Beta step ...")
        self.beta_list = {}
        self.XT_X_matrix_inverse = {}
        XT_X_matrix = {}
        XT_Y_vector = {}
        for mySNP in client_XT_X_matrix.keys():
            XT_X_matrix[mySNP] = np.zeros((len(client_XT_Y_vector[mySNP]), len(client_XT_Y_vector[mySNP])))
            XT_Y_vector[mySNP] = np.zeros((len(client_XT_Y_vector[mySNP]), 1))

                #for client_parameter in parameters_from_clients:
                    #client_XT_X_matrix = client_parameter[Parameter.XT_X_MATRIX][genotype_index]
                    #client_XT_Y_vector = client_parameter[Parameter.XT_Y_VECTOR][genotype_index]

                    #XT_X_matrix = np.add(XT_X_matrix, client_XT_X_matrix)
            XT_X_matrix[mySNP] = np.add(XT_X_matrix[mySNP], client_XT_X_matrix[mySNP])
                    #XT_Y_vector = np.add(XT_Y_vector, client_XT_Y_vector)
            XT_Y_vector[mySNP] = np.add(XT_Y_vector[mySNP], client_XT_Y_vector[mySNP])

                #if np.linalg.det(XT_X_matrix) == 0:
                #    self.genotype_unignored_index_set.discard(genotype_index)
                #    continue

            self.XT_X_matrix_inverse[mySNP] = np.linalg.inv(XT_X_matrix[mySNP])
            beta_vector = np.dot(self.XT_X_matrix_inverse[mySNP], XT_Y_vector[mySNP])
            self.beta_list[mySNP] = beta_vector
        #print("Beta step aggregation done!")
        #print("beta")
        #print(self.beta_list)
        return self.beta_list

    #sPLINK stdErr aggregation step
    def std_error_step_aggregation(self, real_predicted_Y_sum_square_error, non_missing_sample_count):
        #print("Aggregation of std error step ...")
        self.std_error_list = {}
        self.non_missing_sample_count = non_missing_sample_count
        sum_square_error = 0
            #for client_parameter in parameters_from_clients:
        for mySNP in real_predicted_Y_sum_square_error.keys():
            #self.non_missing_sample_count[mySNP] = non_missing_sample_count
            client_sum_square_error = real_predicted_Y_sum_square_error[mySNP]
            sum_square_error += client_sum_square_error

            #sigma_squared = sum_square_error / (self.non_missing_sample_count[mySNP] - len(self.confounding_features.split(",")) - 1)
            sigma_squared = sum_square_error / (self.non_missing_sample_count[mySNP] - 4)
            variance_vector = (sigma_squared * self.XT_X_matrix_inverse[mySNP]).diagonal()
            std_error_vector = np.sqrt(variance_vector)
            self.std_error_list[mySNP] = std_error_vector

        #print("stdErr: ")
        #print(self.std_error_list)
        self.compute_t_stat()

        self.compute_p_value()

        #self.init_ignored_genotypes()

    #sPLINK stdErr -> tStat
    def compute_t_stat(self):
        #print("Computing T statistics ...")
        self.t_stat_list = {}
        for mySNP in self.beta_list.keys():
            #self.t_stat_list[mySNP] = (self.beta_list[mySNP] /np.transpose(self.std_error_list[mySNP]))
            self.t_stat_list[mySNP] = (np.transpose(self.beta_list[mySNP]) /self.std_error_list[mySNP])
        #print(self.t_stat_list)

    #sPLINK tStat -> pVal
    def compute_p_value(self):
        #print("Computing P values ...")
        self.p_value_list = {}
        for mySNP in self.beta_list.keys():
            #degree_of_freedom = self.non_missing_sample_count[mySNP] - len(self.confounding_features.split(",")) - 1
            degree_of_freedom = self.non_missing_sample_count[mySNP] - 4
            self.p_value_list[mySNP] = 2 * (1 - stats.t.cdf(np.abs(self.t_stat_list[mySNP]), degree_of_freedom))
        #print("p-vals")
        #print(self.p_value_list)

    #compose Output
    def constructLinRegOutput(self):#, SNP_names, chromosomes
        print("CHR,SNP,BP,A1,TEST,NMISS,BETA,STAT,P")
        for mySNP in self.beta_list.keys():
            myChr = -1
            myBP = -1
            myA1 = -1
            print(str(myChr)+","+mySNP+","+str(myBP)+","+str(myA1)+",ADD,"+str(self.non_missing_sample_count[mySNP])+","+str(self.beta_list[mySNP].item(1))+","+str(self.t_stat_list[mySNP].item(1))+","+str(self.p_value_list[mySNP].item(1)))
            for i in range(len(self.confounding_features.split(","))):
                print(str(myChr)+","+mySNP+","+str(myBP)+","+str(myA1)+","+self.confounding_features.split(",")[i]+","+str(self.non_missing_sample_count[mySNP])+","+str(self.beta_list[mySNP].item(2+i))+","+str(self.t_stat_list[mySNP].item(2+i))+","+str(self.p_value_list[mySNP].item(2+i)))
            #print(self.t_stat_list[mySNP])
            #print(self.p_value_list[mySNP])
        return


if __name__ == '__main__':

    # test
    client = Client(Server("Age,Sex", "Lin_Reg"), "../../toy_logistic_split1.bed", "../../toy_logistic_split1.fam", "../../toy_logistic_split1.cov",
                    "../../toy_logistic_split1.bim", "Age,Sex", 6, "Lin_Reg")


# TODO: what is the encoding for the genotype? PLINK reference != sPLINK code != sPLINK paper
# TODO: maybe implement a faster, asynchronous, parallelization (with function that sorts the results in the end)
# TODO: Do missing samples have to be ignored when calculating allele counts?
